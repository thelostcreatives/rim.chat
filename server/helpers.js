// from https://medium.com/javascript-in-plain-english/how-to-deep-copy-objects-and-arrays-in-javascript-7c911359b089
const deepCopy = obj => {
  let clone, value, key;

  if(typeof obj !== "object" || obj === null) {
    return obj; // Return the value if obj is not an object
  }

  // Create an array or object to hold the values
  clone = Array.isArray(obj) ? [] : {};

  for (key in obj) {
    value = obj[key];

    // Recursively (deep) copy for nested objects, including arrays
    clone[key] = (typeof value === "object" && value !== null) ? deepCopy(value) : value;
  }
  
  return clone;
}

module.exports = {
    deepCopy
}
