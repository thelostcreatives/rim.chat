require('dotenv').config();
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');
const { setup } = require('radiks-server');

const { deepCopy } = require('./helpers');

var whitelist = ['http://localhost:3000', 'http://127.0.0.1:3000', 'https://dev.rim.chat', 'https://rim.chat']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

const app = express();

app.use(cors(corsOptions));

setup({
    mongoDBUrl: process.env.MONGODB_URI
}).then(RadiksController => {
    app.use('/radiks', RadiksController);
});

app.get("/", (req, res) => {
    res.send('Wazzzaaaa');
})

const server = app.listen(process.env.PORT || 5000);
const io = socketio.listen(server);

let onlineUsers = {};

const onlineVal = 1;

io.on('connection', (socket) => {
  
    let username = socket.handshake.query.username;

    socket.on('subscribe', function(room) { 
        socket.join(room); 

        onlineUsers = {
            ...onlineUsers,
            [room]: onlineUsers[room] ? {...onlineUsers[room], [username]: onlineVal} : {[username]: onlineVal}
        }
        io.in(room).emit(`${room}-online`, onlineUsers[room]);
    })

    socket.on('unsubscribe', function(room) {  
        socket.leave(room); 
    })

    socket.on('send', (data) => {
        socket.broadcast.in(data.room).emit('message', data.msg);
    });

    socket.on('disconnect', () => {
        let updatedUsers = deepCopy(onlineUsers);

        Object.keys(updatedUsers).forEach(room => {
            if (updatedUsers[room][username]) {
                delete updatedUsers[room][username];
                io.in(room).emit(`${room}-online`, updatedUsers[room]);
            }
        });

        onlineUsers = updatedUsers;
    });
})

