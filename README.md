# Rim.chat

Rim.chat is an opensource app chat app with decentralized storage built with Blockstack.

# Contributing

## Setup
- Make sure `node` is available. (Using `yarn` as the package manager) 
- Need a [Blockstack](https://blockstack.org/) account 

### React Setup

Install dependencies:
```
yarn install
```

In the `server` directory, create a `.env` file with the content:
```
MONGO_DB_URL="mongodbUrl"
```

`cd` into the `server` directory and install dependencies:
```
cd server
yarn install
```

Start react app: 
```
//in root directory
yarn start
```
Available at http://127.0.0.1:3000/ by default. You can change this to `localhost` in the `.env.development` file

Start server:
```
//in server directory
node index.js
```

Then start making your changes :)
