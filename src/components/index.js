export { default as Main } from './Main';
export { default as Profile} from './Profile';
export { default as About } from './About';
export { default as Signin } from './Signin';
export { default as Chats } from './Chats';
export { default as ChatGroup } from './ChatGroup';
export { default as Messages } from './Messages';
export { default as MessageInput } from './MessageInput';
export { default as ImageCarousel } from './ImageCarousel';
export { default as MessageRequests } from './MessageRequests';

export * from './minor_comps';
