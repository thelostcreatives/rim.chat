import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { XCircle } from 'react-feather';

import { 
    updateUser, setActiveGroup, getGroupInvitations,
    getGroup, activateInvitation, updateInvitation, 
    toggleCreatingNewConvo
} from '../actions';

import { 
    updateChatGroupRef
} from '../utils/helpers';

const MessageRequests = (props) => {
    const { 
        user, default_invitations, gettingInvitations,
        invitationsBy
    } = props;

    const { 
        close, setActiveGroup, activateInvitation,
        updateUser, getGroup, updateInvitation,
        getGroupInvitations, toggleCreatingNewConvo
    } = props;

    const { username } = user.attrs;

    const [invitations, setInvitations] = useState(default_invitations);

    useEffect(() => {
        if (invitations.length === 0) {
            getGroupInvitations(username);
        }
    }, []);

    useEffect(() => {
        setInvitations(invitationsBy ? invitationsBy : default_invitations);
    }, [default_invitations, invitationsBy]);

    const handleInvitationClick = async (invitation) => {
        toggleCreatingNewConvo();

        await activateInvitation(invitation);

        updateInvitation(invitation, {activated: true});

        const newGroup = await getGroup(invitation.attrs.userGroupId); 

        const { _id, members } = newGroup.attrs;

        const memberNames = members.map(member => member.username);

        updateUser( user, {
            chatGroupRef: updateChatGroupRef(user, memberNames, _id), 
            GROUP_MEMBERSHIPS_STORAGE_KEY: JSON.parse(localStorage.getItem('GROUP_MEMBERSHIPS_STORAGE_KEY'))
        });

        setActiveGroup(newGroup, true);

        toggleCreatingNewConvo();

    }

	return (
        <MessageRequestsWrapper onClick = {e => e.stopPropagation()}>
            <XCircle onClick = {close} className = "closeButton"/>
            <div className = "list">
            {
                invitations.length > 0 && !gettingInvitations ?
                invitations.map(invitation => {
                    const { _id, by } = invitation.attrs;

                    return (
                        <div 
                            className = "request"
                            key = {_id} 
                        >
                            <div>{by}</div>
                            <div
                                className = "acceptButton"
                                onClick = {() => handleInvitationClick(invitation)}>
                             Accept
                            </div> 
                        </div>
                    )
                 })
                :
                <h2 className = "noRequestsMessage">
                   No Message Requests 
                </h2>

            }		
            </div>
        </MessageRequestsWrapper>
	)
}

const mstp = (state) => {
    return {
        user: state.auth.User,
        default_invitations: state.group.invitations,
        gettingInvitations: state.group.gettingInvitations
    }
}

export default connect(mstp, {
    setActiveGroup, activateInvitation, updateUser,
    updateInvitation, getGroup, getGroupInvitations,
    toggleCreatingNewConvo
})(MessageRequests);

const MessageRequestsWrapper = styled.div`
    background: white;
    border-radius: 10px;
    padding: 5px;
    height: fit-content;
    width: 500px;

    .list {
        display: flex;
        flex-direction: column;

        width: 100%;
        height: 500px;
        overflow: auto;
    }

    .noRequestsMessage {
        align-self: center;
    }

    .closeButton {
        float: right;
        margin: 5px;

        &: hover {
            cursor: pointer;
        }
    }

    .request {
        display: flex;
        align-items: center;
        justify-content: space-between;

        padding: 5px;
        margin: 2px;

        height: 50px;
        width: fill-available;
        border-radius: 5px;
        background-color: #E5F4EA;
    }

    .acceptButton {
        color: white;
        background-color: black

        padding: 5px;
        border-radius: 5px;

        &:hover {
            cursor: pointer;
        }
    }
`
