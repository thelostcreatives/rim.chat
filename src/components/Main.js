import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, withRouter } from 'react-router-dom';
import styled from 'styled-components';
import io from 'socket.io-client';
import { connect } from 'react-redux';

import { setActiveGroup, setSocket } from '../actions';
import { Messages, Chats } from './index';
import { breakpoint } from '../utils/styleConsts';

const Main = (props) => {

    const { user, socket } = props;
    const { setSocket } = props;

    const { username } = user.attrs;

    const [isProfileVisible, setIsProfileVisible] = useState(false);
    const [isHelpVisible, setIsHelpVisible] = useState(false);
    const [isAboutVisible, setIsAboutVisible] = useState(false);

    useEffect (() => {
        if (username) {
            const newSocket = io(process.env.REACT_APP_SERVER ? process.env.REACT_APP_SERVER : 'http://localhost:5000/',
                {
                    query: {
                        username: username
                    }
                }
            )
            setSocket(newSocket);
        }
    },[username])
    
    useEffect (() => {

        

    }, []);

    const viewHieght = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    return socket ? (
        <Router>
            <MainWrapper height = {viewHieght}>
                
                <Chats height = {viewHieght}/>
                <Messages />

            </MainWrapper>
        </Router>
    )
    :
    <div>
        LOADING
    </div>
}

const mstp = (state) => {
    return {
        user: state.auth.User,
        socket: state.auth.socket
    }
}

export default connect(mstp, {setActiveGroup, setSocket})(withRouter(Main));

const MainWrapper = styled.div`
    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: 'Work Sans', sans-serif;
    }

    display: grid;
    display: grid;
    grid-template-columns: minmax(360px, 1fr) 4fr;

    height: ${props => `${props.height}px`};
    

    a {
        color: black;
        &:hover{
            color: grey;
        }
    }

    #chats {
        background-color: red;
    }

    #messages {
        background-color: blue;
    }

    #nav {
        margin-left: 70px;
    }

    #main {
        display: flex;
        flex-direction: column;
        justify-content: center;
        position: relative;

        #help-image {
            position: absolute;
            top: auto;
            right: 100%;
            width: 500px;
        }
    }


    #aside {
        position: absolute;
        right: 0;
    }

    #share-btns { 
        margin: 10px 0;
        margin-left: 70px;
    }

    @media only screen and (max-width: ${breakpoint.a}) {
        // grid-template-columns: auto;
        // #chats {
        //     display: none;
        // }

        #main {
            
            #help-image {
                position: absolute;
                top: 100%;
                left: 0;
                width: 100%;
            }
        }

    }
`;
