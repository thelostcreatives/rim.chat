import React, { useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { debounce } from 'underscore';

import {
    MessageInput, SearchBar, Message as MessageComp,
    LoadingScreen, FakeLink, MessageRequests,
    Overlay
} from './index';
import { 
    getMessages,toggleCreatingNewConvo, receiveMessage, 
    createSession, countSessions, updateUser, 
    searchUser, deselectUser, getGroup,
    setActiveGroup, getGroupInvitationsBy, clearInvitationsBy
} from '../actions';
import { getAltGroupName, stringifyMembers } from '../utils/helpers';
import { breakpoint } from '../utils/styleConsts';

import { Message } from '../models';


const Messages = (props) => {

    const { 
        user, searchResults, socket, 
        chatMessages, group, isCreatingNewConvo,
        selectedUsers, setActiveGroup, invitationsBy,
        onlineUsers
    } = props;

	const { 
        getMessages, receiveMessage, 
        searchUser, deselectUser,
        getGroup, getGroupInvitationsBy,
        clearInvitationsBy
    } = props;

	const { _id, name, members } = group ? group.attrs : {};

    const onlineMembers = onlineUsers[_id];

    const { 
        license, username, preferences,
        signingKeyId, chatGroupRef
    } = user.attrs;

	const [previousScrollHeight, setPreviousScrollHeight] = useState();
	const [scrollPosition, setScrollPosition] = useState();

    const [altGroupName, setAltGroupName] = useState('');

    const [hasNewConvoDup, setHasNewConvoDup] = useState(false);

	const messages = useRef(document.getElementsByClassName('messages'));
	const anchor = useRef(null);

	const bottomRange = 20;
    const convoNameLimit = 50;

    const showActiveChatGroup = (group._id && chatMessages[group._id] && !isCreatingNewConvo) || (hasNewConvoDup && group._id);
    const showExistingRequests = selectedUsers.length === 1 && invitationsBy.length > 0 && isCreatingNewConvo;

	const loadMore = debounce( () => {
            
            if (!_id) {
                return;
            }

			if (messages.current.scrollTop <= 200) {
				getMessages(chatMessages[group._id].length, 10, group._id)
			}

			setScrollPosition( () => {
				return Math.round(messages.current.scrollHeight - messages.current.scrollTop)
			})
		},
		50
	);

	messages.current.onscroll = loadMore;

    useEffect(() => {

        const setDuplicateAsActive = async (groupId) => {
            const duplicateGroup = await getGroup(groupId);
            setActiveGroup(duplicateGroup);
        }

        if (chatGroupRef) {
            const groupDuplicateId = chatGroupRef[stringifyMembers([username, ...selectedUsers])];
            if (groupDuplicateId) {
                setHasNewConvoDup(true);
                setDuplicateAsActive(groupDuplicateId);
            } else {
                setHasNewConvoDup(false);
            }
        }

    }, [selectedUsers]);

    useEffect(() => {

        if(selectedUsers.length === 1) {
            getGroupInvitationsBy(selectedUsers[0], username);
        }

    }, [selectedUsers]);

    useEffect(() => {

        const { members } = group.attrs;
        if (members){
            setAltGroupName(getAltGroupName(members, username));
        }

    }, [group.attrs]);

	useEffect (() => {

		if (!_id) return;

        if (chatMessages[_id]) {
			anchor.current.scrollIntoView({ behavior: "smooth"})
		} 

		if ( !(scrollPosition >= messages.current.clientHeight - bottomRange && scrollPosition <= messages.current.clientHeight + bottomRange) ) {
			setPreviousScrollHeight(prev => {
				if (prev) {
					messages.current.scrollTop = messages.current.scrollHeight - prev;
				} 
				return messages.current.scrollHeight;
			})
		}

	}, [group, chatMessages[group._id], isCreatingNewConvo])

	useEffect (() => {
        if (socket) {
            socket.on('message', async (msg) => {

                const message = new Message(msg);
                await message.decrypt();

                receiveMessage(message);
            });
        }
	},[socket])
	
	return ( 
		<MessagesWrapper> 
			<div id = "infoBar">
                {
                    isCreatingNewConvo ?
					<SearchBar 
						placeholder = "Search Users" selectedItems = {selectedUsers} searchResults = {searchResults}
						removeItem = {deselectUser} onChange = {searchUser} wrap = "true"
					/>
                    :
                    <h5 className = "convoName" title = {altGroupName}>
                        { 
                            name ? 
                                name
                                :
                                altGroupName.length > convoNameLimit ? 
                                    `${altGroupName.substr(0, convoNameLimit)}...`
                                    :
                                    altGroupName
                        }
                    </h5>
                }
			</div>
			<div className = "messagesContainer">
				<div ref = {messages} className = "messages">
					{
						 showActiveChatGroup ?
							chatMessages[group._id].map((message, idx, messages) => {
								const { author } = message.attrs;

								const isLast = idx === messages.length - 1
								let isOwned = author === username;
								let isSingleMessage;
								let isStartOfGroup = messages.length > 1 && idx < messages.length - 1;
								let isEndOfGroup = messages.length > 1 && idx === messages.length - 1;

								if(idx > 0 && idx < messages.length - 1) {
									const { author: prevAuthor } = messages[idx - 1].attrs;
									isStartOfGroup = author !== prevAuthor;
								}

								if(idx < messages.length - 1) {
									const { author: nextAuthor } = messages[idx + 1].attrs;
									isEndOfGroup = author !== nextAuthor;
								}

								if (isEndOfGroup && isLast) {
									const { author: prevAuthor } = messages[idx - 1].attrs;
									isEndOfGroup = author === prevAuthor;
								} 

								if (isStartOfGroup && isEndOfGroup) {
									isStartOfGroup = false;
									isEndOfGroup = false;
									isSingleMessage = true;
								}

								return <MessageComp 
										key = {message._id} 
										message={message} 
										isOwned = {isOwned} 
                                        onlineMembers = {onlineMembers}
										isStartOfGroup = {isStartOfGroup} 
										isEndOfGroup = {isEndOfGroup}
										isLast = {isLast}
										isSingleMessage = {isSingleMessage}
									/>
							})
							:
							null
					}
                    {
                        showExistingRequests ?
                        <Overlay 
                            component = {<MessageRequests invitationsBy = {invitationsBy} close = {() => clearInvitationsBy()}/>} 
                            zIndex = {0}
                        />
                        :
                        null
                    }
					<div id = "anchor" ref = {anchor}></div>
				</div>
			</div>
            {
                !showExistingRequests ?
                <MessageInput socket = {socket} hasDuplicate = {hasNewConvoDup} />
                :
                null
            }
		</MessagesWrapper>
	)
}

const mstp = (state) => { 
	return { 
		user: state.auth.User,
        socket: state.auth.socket,
		userSession: state.auth.userSession,
        searchResults: state.auth.searchResults,
		tracks: state.tracks.allTracks,

        group: state.group.activeGroup,
        onlineUsers: state.group.onlineUsers,
        selectedUsers: state.group.selectedUsers,
        invitationsBy: state.group.invitationsBy,
		sessionCount: state.sessions.count,
		chatMessages: state.message.chatGroups,
		isCreatingNewConvo: state.group.isCreatingNewConvo,
		hasMore: state.message.hasMore
	}
}

export default connect(mstp, {
    getMessages, toggleCreatingNewConvo, receiveMessage, 
    createSession, countSessions, updateUser, 
    searchUser, deselectUser, getGroup,
    setActiveGroup, getGroupInvitationsBy, clearInvitationsBy
})(Messages);

const MessagesWrapper = styled.div`
	display: flex;
	flex-direction: column;
	width:100%;

	// align-items: center;
	// justify-content: flex-end;

	#infoBar {
		display: flex;
		align-items: center;
		justify-content: space-between;

        position: relative;


		min-height: 50px;
        padding: 5px 0;
		border-bottom: 1px solid rgba(232, 237, 237, 0.56);
	}

    .convoName {
        margin: 0 10px;
    }

	.messagesContainer {
		position: relative;
		height: 100%;
		// height: 700px;
		// overflow: auto;

	}

	.messages > :first-child {
		margin-top: auto !important;
	}

	.messages {
		position: absolute;
		top: 0;
		bottom: 0;
		width: 100%;
		overflow: auto;

		::-webkit-scrollbar {
			background-color: none;
			width: 8px;
			border-radius: 10px;
			&: hover {
				background-color: rgba(115, 129, 150, 0.3);
			}
		}


		::-webkit-scrollbar-thumb {
			// background-color: #738196;
			background-color: #B0B4B4;
			border: none;
			border-radius: 10px;
		}
	}

	.messages * {
		overflow-anchor: none;
	}

	#anchor {
		overflow-anchor: auto;
		height: 1px;
	}

    @media only screen and (max-width: ${breakpoint.b}) {
		width: 100%;
		.volumeSliders {
			width: 200px;
		}
	}
`
