import React, { } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { handleSignIn } from '../actions';
import logo from '../imgs/socialli_no_bg.png'

const SignIn = (props) => {

    return (
      <SigninWrapper>
        <div className = "section">
          <img src = {logo} alt = "logo" id = "logo"/>
          <div className = "heading">
            <h1>rim.chat</h1>
          </div>
          <button
              onClick={ (e) => props.handleSignIn(e, props.userSession)}
              className = "signin-button"
          >
            Sign In with Blockstack
          </button>
        </div>
      </SigninWrapper>
    );
}

const mstp = state => {
    return {
        userSession: state.auth.userSession
    }
}

export default connect(mstp, {handleSignIn})(SignIn);

const SigninWrapper = styled.div`
  display: flex;
  flex-direction: column;

  align-items: center;
  justify-content: center;

  font-family: 'Work Sans', sans-serif;

  #logo {
    width: 200px;
  }
  .heading {
    display: flex;
    flex-direction: column;
    align-items: center;
    h1 {
      font-size: 50px;
      margin-bottom: 0;
    }

    margin-bottom: 50px;
  }

  .section {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    min-height: 45vh;
    width: 100%;

    p {
      margin: 10px 0;
      max-width: 500px;
      line-height: 1.5;
    }
  }

  .signin-button {
    font-size: 15px;
    padding: 10px;
    background: #29356d;
    color: white;

    transition-duration: .5s;
    border-radius: 5px;

    &:hover {
      cursor: pointer;
      background: #409eff;
    }
  }
  
  .footer {
    margin: 50px 0;
    max-width: 500px;
    width: 100%;
  }
`;
