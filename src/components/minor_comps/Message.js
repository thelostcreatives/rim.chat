import React, { useEffect, useState, useRef} from 'react';
import { Editor, EditorState, ContentState, SelectionState, Modifier, convertToRaw, getDefaultKeyBinding, convertFromRaw} from 'draft-js';
import styled from 'styled-components';

import { removeExtraNewLines, getCompositeDecorator, HANDLE_REGEX, HASHTAG_REGEX, getMatchesFromString } from '../../utils/helpers';

const Message = (props) => {

    const { 
        message, bgColor, isOwned, 
        isStartOfGroup, isEndOfGroup, isLast, 
        isSingleMessage, onlineMembers
    } = props;
	const { onClick } = props;

	const { author, content } = message.attrs;

    const isOnline = onlineMembers ? onlineMembers[author] : false;

	const editor = useRef(null);
	const postDecorator = getCompositeDecorator('post');
    // const editorDecorator = getCompositeDecorator('editor');

    const newEditorState = EditorState.createWithContent(convertFromRaw(content), postDecorator);

    const [editorState, setEditorState] = useState(newEditorState);

	// const handleEditorChange = (editorState) => { 
    //     setEditorState(editorState);
	// }

    return (
        <MessageWrapper onClick = { onClick } bgColor = { bgColor } {...props}>
			{
				isStartOfGroup || isSingleMessage || (isLast && !isStartOfGroup && !isEndOfGroup) ?
                    <Name isOwned = {isOwned}>
                        {
                            isOwned ? 
                                isOnline ?
                                    "● You" 
                                    : 
                                    "You" 
                                : 
                                isOnline ? 
                                    `${author} ●` 
                                    : 
                                    author
                        }
                    </Name>
					:
					null
			}
			<div className = "message" >
				<Editor
					ref = {editor}
					editorState = {editorState}
					// onChange = {handleEditorChange}
					readOnly = {true}
				/>
			</div>
		</MessageWrapper>
			
    )
}

export default Message;

const MessageWrapper = styled.div`

	display: flex;
	flex-direction: column;
	margin: 2px 10px;
	margin-top: ${props => props.isStartOfGroup || props.isSingleMessage ||  (props.isLast && !props.isEndOfGroup)  ? "30px" : "unset"};
	// align-self: ${props => props.isOwned ? "flex-end" : "flex-start"};

	width: fill-available;
	.message {
		background-color: ${props => props.isOwned ? "#EB4228" : "#E8EDED"};
		color: ${props => props.isOwned ? "white" : "black"};
		max-width: 65%;
		padding: 6px 12px 7px;
		width: fit-content;
		border-bottom-right-radius: ${props => props.isOwned && props.isStartOfGroup ? "5px" : "18px"}
		border-top-right-radius: ${props => props.isOwned && props.isEndOfGroup ? "5px" : "18px"}
		border-bottom-left-radius: ${props => !props.isOwned && props.isStartOfGroup ? "5px" : "18px"}
		border-top-left-radius: ${props => !props.isOwned && props.isEndOfGroup ? "5px" : "18px"}
		height: fit-content;
		align-self: ${props => props.isOwned ? "flex-end" : "flex-start"};
	}
`;

const Name = styled.span`
    font-size: 12px;
    display: block;
    padding: 1px 10px;
    align-self: ${props => props.isOwned ? "flex-end" : "flex-start"};
    color: ${props => props.isOnline ? "green" : "#AFB7C3"};
`;
