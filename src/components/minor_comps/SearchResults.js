import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { UserPreview } from './index';

const SearchResults = (props) => {
    
    const { username, searchResults, selectedUsers, onSelect } = props;

    const cleanResults = searchResults.filter(user => {
        const _username = user.attrs.username;
        return _username !== username && !selectedUsers.includes(_username);
    });

	return (
		<SearchResultsWrapper>
            {
                cleanResults.map((user, idx) => {
                    return <UserPreview key = {user._id} idx = {idx} onSelect = { onSelect } {...user}/>
                })
            }
		</SearchResultsWrapper>
	);
};

const mstp = (state) => { 
	return { 
        username: state.auth.User.attrs.username,
	    searchResults: state.auth.searchResults,
        selectedUsers: state.group.selectedUsers
	}
}

export default connect(mstp, {})(SearchResults);

const SearchResultsWrapper = styled.div`
    width: fit-content;

    z-index: 10;

    position: absolute;
    left: 2%;
    top: 80%;

    background-color: white;
    border-radius: 5px;
    padding: 5px 0;
    max-height: 300px;

    overflow: auto;

    -webkit-box-shadow: 0px 0px 8px -1px rgba(153,153,153,1);
    -moz-box-shadow: 0px 0px 8px -1px rgba(153,153,153,1);
    box-shadow: 0px 0px 8px -1px rgba(153,153,153,1);
`
