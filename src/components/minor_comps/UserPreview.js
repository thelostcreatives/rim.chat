import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { selectUser } from '../../actions';

const UserPreview = (props) => {
    
    const { attrs } = props;
    const { selectUser, onSelect } = props;

    const { username } = attrs;

    const handleClick = () => {
        onSelect();
        selectUser(username)
    }

	return (
        <UserPreviewWrapper onClick = {handleClick}>
            <span>{username}</span> 
		</UserPreviewWrapper>
	);
};

const mstp = (state) => { 
	return { 
	    searchResults: state.auth.searchResults
	}
}

export default connect(mstp, {selectUser})(UserPreview);

const UserPreviewWrapper = styled.div`

    padding: 5px 10px;

    &:hover {
        cursor: pointer;
        background-color: #eaecee;
    }
    
`
