import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import styled from 'styled-components';


const Overlay = (props) => {
	const { component: Component, zIndex = 100 } = props;

	const { close } = props;

	return (
        <OverlayWrapper onClick = {close} zIndex = {zIndex}>
                {
                    Component
                }
		</OverlayWrapper>
	);
};

const mstp = (state) => { 
	return { 
		
	}
}

export default connect(mstp, {})(Overlay);

const OverlayWrapper = styled.div`
    background-color: rgba(0, 0, 0, .3);

    z-index: ${props => props.zIndex};


	display: flex;
    align-items: center;
    justify-content: center;

	
    position: absolute;
    width: 100%;
    height: 100%;
	
`;
