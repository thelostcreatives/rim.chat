import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { debounce } from 'underscore';

import { SearchResults } from './index';
import { setSearchString, searchUser, searchMessages } from '../../actions';

const SearchBar = (props) => {

    const { selectedItems = [], searchResults = [], placeholder, background, wrap } = props;

	const { searchUser, searchMessages, onChange, removeItem } = props;

    const [searchString, setSearchString] = useState("");

    const debouncedOnChange = debounce(
        (inputString) => onChange(inputString),
        700
    );

    const handleInputChange = (e) => {
        const inputString = e.target.value; 
        setSearchString(inputString);
        debouncedOnChange(inputString);
    }

    const clearSearchString = () => {
        setSearchString("");
    }

    const handleDelete = (e) => {
        if (e.keyCode === 8 && e.target.value.length === 0 && selectedItems.length > 0) {
            removeItem(selectedItems.length - 1);
        }
    }

	return (
		<SearchBarWrapper background = { background } wrap = {wrap}>
            {
                selectedItems.length > 0 ?
                selectedItems.map(item => {
                    return <div key = {item} className = "item">{item}</div>
                })
                :
                null
            }
            <input value = { searchString } onChange = { handleInputChange } placeholder = { placeholder } onKeyDown = { handleDelete }/>
            {
                searchResults.length > 0 ?
                <SearchResults onSelect = { clearSearchString } />
                :
                null
            }
		</SearchBarWrapper>
	);
};

const mstp = (state) => { 
	return { 
        selectedUsers: state.group.selectedUsers
	}
}

export default connect(mstp, {setSearchString, searchUser, searchMessages})(SearchBar);

const SearchBarWrapper = styled.div`

    width: 100%;
    display: flex;
    align-items: center;
    flex-wrap: wrap;


    min-height: 35px;
    width: fill-available;
    margin: 5px;
    padding: 0 5px;
    border-radius: 50px;
    background-color: ${props => props.background};

    *{
        font-size: 15px;
    }

    input {

        border: none;
        outline: none;

        background: none;
    
        width: ${props => props.wrap ? "unset" : "fill-available"} ;
        padding: 0 8px;
        margin: 2px 0;
        text-align: left;
    }

    .item {
        background-color: #84b9ff;
        padding: 2px;
        margin: 2px;
        border-radius: 5px;
    }
	
`
