import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { Editor, EditorState, ContentState, SelectionState, Modifier, convertToRaw, getDefaultKeyBinding, KeyBindingUtil } from 'draft-js';
import styled from 'styled-components';
import { Image, PlayCircle, Music, Send } from 'react-feather';

import { ImageCarousel, Video, Audio } from './index';
import { 
    updateUser, createGroup, inviteMembers, 
    createMessage, uploadImages, uploadVideo, 
    uploadAudio, createNotif, toggleCreatingNewConvo,
    setActiveGroup
} from '../actions';
import { breakpoint } from '../utils/styleConsts';
import { 
    isImageFileSizeAcceptable, areAllImageFileSizesAcceptable, compressImage, 
    removeExtraNewLines, getCompositeDecorator, HANDLE_REGEX, 
    HASHTAG_REGEX, getMatchesFromString, updateChatGroupRef,
} from '../utils/helpers';
import { 
    SUPPORTED_IMAGE_FORMATS, SUPPORTED_VIDEO_FORMATS, SUPPORTED_AUDIO_FORMATS,
    NOTIF_TYPES
} from '../utils/constants';

const MessageInput = (props) => {
	const { 
		rimUser, userSession, socket, 
        group, selectedUsers, isCreatingNewConvo,
        hasDuplicate
	} = props;

	const {
        updateUser, createGroup, inviteMembers,
        createMessage, uploadImages, uploadVideo,
        uploadAudio, createNotif, toggleCreatingNewConvo,
        setActiveGroup
	} = props;

	const { username } = rimUser.attrs;
	const { _id } = group ? group.attrs : {};

	const { hasCommandModifier } = KeyBindingUtil;

	const [images, setImages] = useState();
	const [tempImgUrls, setTempImgUrls] = useState();

	const [video, setVideo] = useState();
	const [tempVideoUrl, setTempVideoUrl] = useState();

	const [audio, setAudio] = useState();
	const [tempAudioUrl, setTempAudioUrl] = useState();

	const [creatingMessage, setCreatingMessage] = useState(false);

	const [isEmojiPickerVisible, setIsEmojiPickerVisible] = useState(false);

	const [editorState, setEditorState] = useState(EditorState.createEmpty(getCompositeDecorator('editor')));

	const editor = useRef(null);

	const focusEditor = () => {
		editor.current.focus();
	}

	useEffect (() => {
		focusEditor();
	}, []);

	useEffect(() => {
		return () => {
			if ( tempImgUrls ) {
				tempImgUrls.forEach( url => window.URL.revokeObjectURL(url) );
			}
		}
	},[tempImgUrls]);
	
    const handleCreateMessage = async (group, cleanText, cleanContentState) => {
        setCreatingMessage(true);
        let imageGaiaLinks = images;
        let videoGaiaLink = video;
        let audioGaiaLink = audio;
        const mentions = getMatchesFromString(HANDLE_REGEX, cleanText).map(mention => mention.substr(1));
        const hashtags = getMatchesFromString(HASHTAG_REGEX, cleanText).map(hashtag => hashtag.substr(1));

        if ( images ) {
            imageGaiaLinks = await uploadImages(userSession, rimUser, images);
        }

        if (video) {
            videoGaiaLink = await uploadVideo(userSession, rimUser, video);
        }

        if ( audio ) { 
            audioGaiaLink = await uploadAudio(userSession, rimUser, audio);
        }

        const newMessage = await createMessage(
            group._id,
            username,
            convertToRaw(cleanContentState),
            mentions,
            hashtags,
            imageGaiaLinks,
            videoGaiaLink,
            audioGaiaLink
        );

        const encrytedData = await newMessage.encrypted()

        socket.emit('send', { msg: encrytedData, room: group._id });
    }

	const handleMessage = async () => {

        if (isCreatingNewConvo && selectedUsers.length === 0) {
            return
        }

		const contentState = editorState.getCurrentContent(); 
		
        if (contentState.hasText()) {

            const cleanText = removeExtraNewLines(contentState.getPlainText());
            const cleanContentState = ContentState.createFromText(cleanText);

            if (isCreatingNewConvo && !hasDuplicate) {
                const newGroup = await createGroup();

                await inviteMembers(username, newGroup, selectedUsers);

                await updateUser( rimUser, {
                    chatGroupRef: updateChatGroupRef(rimUser, [username, ...selectedUsers], newGroup._id),
                    GROUP_MEMBERSHIPS_STORAGE_KEY: JSON.parse(localStorage.getItem('GROUP_MEMBERSHIPS_STORAGE_KEY')),
                });

                await handleCreateMessage(newGroup, cleanText, cleanContentState);

                setActiveGroup(newGroup, true);
                toggleCreatingNewConvo();

            } else {
                handleCreateMessage(group, cleanText, cleanContentState);
            }

			
			// done();
			// followMessage(anylistUser, newMessage._id);

			// createNotif(username, "_", newMessage._id, NOTIF_TYPES.post, { 
			// 	...newMessage.attrs.metadata
			// }, mentions);
		} else {
			console.log("Tell us stories meyn");
		}
	}

	const toggleEmojiPicker = () => {
		setIsEmojiPickerVisible(!isEmojiPickerVisible);
	}

	const resetEditor = () => {
		const contentState = editorState.getCurrentContent();
		const firstBlock = contentState.getFirstBlock();
		const lastBlock = contentState.getLastBlock();
		const selection = new SelectionState({
			anchorKey: firstBlock.getKey(),
			anchorOffset: 0,
			focusKey: lastBlock.getKey(),
			focusOffset: lastBlock.getLength(),
			hasFocus: true
		});;
		const newState =  Modifier.removeRange(contentState, selection, 'backwards')
		const state = EditorState.push(editorState, newState, "insert-characters");
		setEditorState(state);
	}

	const handleImageIconClick = () => {
		document.getElementById("image-input").click();
	}

	const handleVideoIconClick = () => {
		document.getElementById("video-input").click();
	}

	const handleAudioIconClick = () => {
		document.getElementById("audio-input").click();
	}

	const handleImageInputChange = async (e) => {
		const files = [...e.target.files].filter( file => {
			const fileNameSplit = file.name.split(".");
			const fileFormat = fileNameSplit[fileNameSplit.length - 1].toLowerCase();
			return SUPPORTED_IMAGE_FORMATS.includes(fileFormat);
		});

		const appendImageToImages = (image) => {
			setImages(images => {
				if (!images) {
					return [image];
				} else {
					return  [...images, image];
				}
			});
		}

		const tempUrls = files.map( file => window.URL.createObjectURL(file) );
		setTempImgUrls(tempUrls.length > 0 ? tempUrls : null);

		if (areAllImageFileSizesAcceptable(files)) {
			setImages(files.length > 0 ? files : null);
		} else {
			files.forEach( (image) => {
				const nameSplit = image.name.split('.');
				const fileType = nameSplit[nameSplit.length - 1];
				if(isImageFileSizeAcceptable(image.size) || fileType === 'gif') {
					appendImageToImages(image);
				} else {
					compressImage(image, (compressed) => {
						appendImageToImages(compressed);
					});
				}
			});
		}
	}

	const handleVideoInputChange = async (e) => {

		const file = e.target.files[0];
		const fileNameSplit = file.name.split(".");
		const fileFormat = fileNameSplit[fileNameSplit.length - 1].toLowerCase();

		if (!SUPPORTED_VIDEO_FORMATS.includes(fileFormat)) {
			return;
		}

		setVideo(file);

		const tempUrl = window.URL.createObjectURL(file);
		setTempVideoUrl(tempUrl);
	}

	const handleAudioInputChange = async (e) => {

		const file = e.target.files[0];
		const fileNameSplit = file.name.split(".");
		const fileFormat = fileNameSplit[fileNameSplit.length - 1].toLowerCase();

		if (!SUPPORTED_AUDIO_FORMATS.includes(fileFormat)) {
			return;
		}

		setAudio(file);

		const tempUrl = window.URL.createObjectURL(file);
		setTempAudioUrl(tempUrl);
	}

	const handleInput = (editorState) => {
		setEditorState(editorState);
	}

	const keyBindingFn = (e) => {
		if (e.keyCode === 13 /* `S` key */ ) {
			if (e.nativeEvent.shiftKey) {
				return 'split-block';
			} else {
				return 'send';
			}
		}
		return getDefaultKeyBinding(e);
	}

	const handleKeyCommands = (command) => {
		if (command === 'send') {
			resetEditor();
			// handleSend(editorState.getCurrentContent().getPlainText())
			handleMessage();
			return 'handled';
		}
		return 'not-handled';
	}

	return (
		<NewMessageFormWrapper onClick = {focusEditor}>
			<div>
			{
				tempImgUrls ? 
				<ImageCarousel imgs = {tempImgUrls}/>
				:
				null
			}
			{
				tempVideoUrl ? 
				<Video src = {tempVideoUrl}/>
				:
				null
			}
			{
				tempAudioUrl ? 
				<Audio src = {tempAudioUrl} />
				:
				null
			}
			</div>
			<div id = "editor">
				{/* <OptionsBar onClick = {e => e.stopPropagation()}>
					<div>
						<Button onClick = {done} text = "Cancel"/>
					</div>
					<div>
						<Music onClick = {handleAudioIconClick} />
						<PlayCircle onClick = {handleVideoIconClick} />
						<Image onClick = {handleImageIconClick} />
					</div>
				</OptionsBar> */}
				<div id = "messageInput">
					<Editor
						ref = {editor}
						editorState = {editorState}
						onChange = {handleInput}
						handleKeyCommand = {handleKeyCommands}
						keyBindingFn = {keyBindingFn}
						placeholder = {"Share your story..."}
						spellCheck = {true}
					/>
				</div>
				<Send onClick = {handleMessage} />
			</div>
			<input type = "file" id = "image-input" accept = "image/*" hidden = 'hidden' onChange = {handleImageInputChange} multiple/>
			<input type = "file" id = "video-input" accept = "video/*" hidden = 'hidden' onChange = {handleVideoInputChange} multiple/>
			<input type = "file" id = "audio-input" accept = "audio/*" hidden = 'hidden' onChange = {handleAudioInputChange} multiple/>

			
		</NewMessageFormWrapper>
	);
}

const mstp = (state) => {
	return {
		rimUser: state.auth.User,
        group: state.group.activeGroup,
		userSession: state.auth.userSession,
        selectedUsers: state.group.selectedUsers,
		isCreatingNewConvo: state.group.isCreatingNewConvo,
	}
}

export default connect(mstp, {
    updateUser, createGroup, inviteMembers, 
    createMessage, uploadImages, uploadVideo, 
    uploadAudio, createNotif, toggleCreatingNewConvo,
    setActiveGroup
})(MessageInput);

const NewMessageFormWrapper = styled.div`

	// align-self: center;
	
	width: 100%;
	height: fit-content;

	padding: 5px;

	// padding: 10px;
	// margin: 25px 0;
	border-radius: 10px;
	.DraftEditor-root{
		// min-height: 100px;
		width: fill-available; 
		// margin: 10px;
	}

	#editor {
		display: flex;
		align-items: flex-end;
	}

	#messageInput {
		background-color: #eaecee;
		width: 100%;
		max-height: 144px;
		min-height: 24px;
		overflow-y: auto;
		margin: 5px;

		border-radius: 18px;

		padding: 8px 15px;
	}

	svg {
		margin: 10px 5px;
		color: #eb4228;
        &:hover {
            color: black;
            cursor: pointer;
        }
	}

	@media only screen and (max-width: ${breakpoint.b}) {
		// width: 90vw;
	}

	@media only screen and (min-width: ${breakpoint.b}) {
		// width: 500px;
	}
`;

export const OptionsBar = styled.div`
	// display: flex;
	justify-content: space-between;
	position: relative;

	& > div {
		display: flex;
	}

	// margin: 5px 0;

	@media only screen and (max-width: ${breakpoint.b}) {
		.emoji-mart {
			left: 0;
			width: 80vw !important;
		}
	}
`;

export const StyledButton = styled.button`
	border: 1px solid ${props => props.bgColor ? props.bgColor : "#599bb3"};
	margin: 5px;
	outline: none;
	background-color: ${props => props.bgColor ? props.bgColor : "#599bb3"} ;
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 5px;
	display: inline-block;
	cursor: pointer;
	color: #ffffff;

	-webkit-font-smoothing: antialiased;
	font-size: 12px;
	line-height: 22px;
	letter-spacing: 1px;
	font-weight: medium;
	padding: 0px 16px;
	text-decoration: none;
	&:hover {
		background-color:  #408c99;
	}
	&:active {
		position: relative;
		top: 1px;
	}
	&:focus {
		border-color: black;
	}
`;
