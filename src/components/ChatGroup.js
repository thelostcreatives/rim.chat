import React, { useEffect, useState } from 'react';
import { convertFromRaw } from 'draft-js';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { 
    getMessages, setActiveGroup, toggleCreatingNewConvo,
    getGroups, updateOnlineUsers
} from '../actions';
import { getAltGroupName } from '../utils/helpers';

const ChatGroup = (props) => {

    const { 
        group, socket, groupMessages, 
        user, isCreatingNewConvo
    } = props;

    const { 
        getMessages, setActiveGroup, toggleCreatingNewConvo,
        getGroups, updateOnlineUsers
    } = props;

	const { attrs } = group;
	const { username } = user.attrs;

	const { _id, name, members } = attrs;

	let groupName = name ? name : getAltGroupName(members, username);
	const isGroup = members.length > 2;

    const messagePreviewLimit = 20;
    const namePreviewLimit = 30;

    const [latestMessage, setLatestMessage] = useState({
        content: "",
        author: null
    });

    useEffect (() => {
        if(!_id) {
            getGroups();
        }
    }, []);

	useEffect (() => {
        if (_id) {
            getMessages(0, 30, _id);
        }
	}, [_id])

    useEffect (() => { 
		if (socket) {
            socket.on(`${group._id}-online`, (users) => {
                updateOnlineUsers(group._id, users);
            })

			socket.emit('subscribe', group._id);
		}

		return () => {
            socket.emit('unsubscribe', group._id);
		}
	}, [socket]);

    useEffect(() => {
        if (groupMessages[_id] && groupMessages[_id].length > 0) {
            const lastIndex = groupMessages[_id].length - 1;

            const { content: rawState, author } = groupMessages[_id][lastIndex].attrs;

            const contentState = convertFromRaw(rawState);
            const rawContent = contentState.getPlainText();

            let content = rawContent;
            if (content.length > messagePreviewLimit) {
                content = `${rawContent.substring(0, messagePreviewLimit)}...`;
            }

            const customAuthor = username === author ? "You" : author;

            setLatestMessage({
                content,
                author: customAuthor === groupName ? '' : customAuthor
            });
        }
    }, [groupMessages, _id]);

	const deleteGroup = async () => {
		// await group.destroy();
        // TODO: figure out where this goes.
	}

    const handleClick = (e) => {
        e.preventDefault();

        if ( isCreatingNewConvo ) {
            toggleCreatingNewConvo();
        }

        setActiveGroup(group);
    }

    return (
        <ChatGroupWrapper onClick = {handleClick} title = {groupName}>
            <h1 id = "name">
                {
                    isGroup && groupName.length > namePreviewLimit ? 
                    `${groupName.substr(0, namePreviewLimit)}...`
                    : 
                    groupName 
                }
            </h1>
            <p>{latestMessage.author ? `${latestMessage.author}:` : null} {latestMessage.content}</p>
		</ChatGroupWrapper>
    )
}

const mstp = (state) => { 
	return { 
		user: state.auth.User,
        socket: state.auth.socket,
		groupMessages: state.message.chatGroups,
        isCreatingNewConvo: state.group.isCreatingNewConvo
	}
}

export default connect(mstp, {
    getMessages, setActiveGroup, toggleCreatingNewConvo,
    getGroups, updateOnlineUsers
})(ChatGroup);

const ChatGroupWrapper = styled.div`

	display: flex;
	flex-direction: column;
	padding: 5px;
	margin: 2px;

	min-height: 50px;
	width: fill-available;
	border-radius: 5px;
	background-color: #E5F4EA;

	&:hover {
		cursor: pointer;
	}

    p {
        opacity: .5;
    }

	#name {
		font-size: 16px;
		font-weight: 400;
		line-height: 1.5;
	}
`;
