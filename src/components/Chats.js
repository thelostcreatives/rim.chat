import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Settings, Edit2 } from 'react-feather';

import { 
    updateUser, toggleCreatingNewConvo, getGroups, 
    setActiveGroup, getGroupInvitations, activateInvitation,
    updateInvitation
} from '../actions';
import { 
    ChatGroup, SearchBar, Overlay, 
    MessageRequests
} from './index';
import { breakpoint } from '../utils/styleConsts';

const Chats = (props) => {

	const { user, groups } = props;
    const { 
        updateUser, toggleCreatingNewConvo, getGroups, 
        setActiveGroup, getGroupInvitations, activateInvitation,
        updateInvitation
    } = props;

    const { username } = user.attrs;

    const [isMoreOptionsVisible, setIsMoreOptionsVisible] = useState(false);
	const [isMessageRequestsVisible, setIsMessageRequestsVisible] = useState(false);

    useEffect (() => {
		const fetchGroups = async () => {
			const groups = await getGroups();
			setActiveGroup(groups[0]);
		}

        if (username) {
            fetchGroups();
        }
	}, [username])
    
    useEffect (() => {
        if (user._id) {
            getGroupInvitations(username);
        }
    }, [user._id]);

    const toggleMoreOptions = (e) => {
        e.preventDefault();
        setIsMoreOptionsVisible(!isMoreOptionsVisible);
    }

    const toggleMessageRequests = (e) => {
        e.preventDefault();
        setIsMessageRequestsVisible(!isMessageRequestsVisible);
	}
	
    // const viewHieght = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    return (
        <Router>
            <ChatsWrapper convos = {groups.length}>
                 <div>
                 {
                     isMessageRequestsVisible ?
                     <Overlay component = {<MessageRequests close = {toggleMessageRequests}/>} close = {toggleMessageRequests}/>
                     :
                     null
                 }
                 </div>
				<div id = "banner">
					<h1>Rim.chat</h1>
				</div>
				<div id = "options">
                    <div className = "icon" onClick = {toggleMoreOptions}> 
						<Settings size = {20}/>
                        {
                            isMoreOptionsVisible ?
                            <div className = "moreOptions" >
                                <div className = "option" onClick= {toggleMessageRequests} >Message Requests</div>
                            </div>
                            :
                            null
                        }
					</div>
					<div className = "icon" onClick = {toggleCreatingNewConvo}> 
						<Edit2 size = {20}/>
					</div>
				</div>
				
                    {/*<SearchBar placeholder = "Search convos" onChange = {() => {}} background = "#eaecee" />*/}

				<div id = "conversations">
					<div id = "chatList">
						{
                            groups.length > 0 ?
							groups.map(group => {
								return <ChatGroup key = {group._id} group = {group}/>
                            })
                            :
                            <h3>No Convos Yet</h3>
						}
					</div>
				</div>
            </ChatsWrapper>
        </Router>
    )
}

const mstp = (state) => { 
	return { 
		user: state.auth.User,
		userSession: state.auth.userSession,
		tracks: state.tracks.allTracks,
        invitations: state.group.invitations,
        groups: state.group.chatGroups,
		sessionCount: state.sessions.count,
		hasMore: state.message.hasMore
	}
}

export default connect(mstp, {
    updateUser, toggleCreatingNewConvo, getGroups, 
    setActiveGroup, getGroupInvitations, activateInvitation,
    updateInvitation
})(withRouter(Chats));

const ChatsWrapper = styled.div`
	display: flex;
	flex-direction: column;

	border-right: 1px solid rgba(232, 237, 237, 0.56);

	#banner {
		display: flex;
		align-items: center;
		box-sizing: border-box;
		height: 50px;
		padding: 10px ;
	}

	#options {
		display: flex;
		justify-content: flex-end;
	}

	.icon {
		display: flex;
		align-items: center;
		justify-content: center;
		padding: 7px;
		margin: 0 5px;
		background-color: #eaecee;
		border-radius: 50px;

        position: relative

		&: hover {
			cursor: pointer;
		}
	}

    .moreOptions {
        position: absolute;
        background-color: white;
        top: 110%;

        z-index: 100;

        width: max-content;
        padding: 5px 0;
        border-radius: 5px;

        box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);
    }

    .option {
        padding: 2px;
        &: hover {
            background-color: #bdbdbd;
            color: white;
        }
    }

	#conversations {
		position: relative;
		height: 100%;
		padding-right: 5px;
	}

	#chatList {
		position: absolute;
		top: 0;
		bottom: 0;
		margin: 10px 0;
		display: flex;
		flex-direction: column;
		align-items: center;
		width: 100%;
		overflow: auto;

		::-webkit-scrollbar {
			background-color: none;
			width: 8px;
			border-radius: 10px;
			&: hover {
				background-color: rgba(115, 129, 150, 0.3);
			}
		}

		::-webkit-scrollbar-thumb {
			background-color: #738196;
			border: none;
			border-radius: 10px;
		}
	}

	#searchInput {
		background-color: #eaecee;
		margin: 5px;

		border: none;
		outline: none;

		border-radius: 50px;
		font-size: 15px;
		height: 35px;
		padding-left: 18px;
		padding-right: 18px;
		text-align: left;
	}

    @media only screen and (max-width: ${breakpoint.a}) {

    }
`;
