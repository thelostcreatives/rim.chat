import { EditorState, convertToRaw } from 'draft-js';
import * as actions from '../actions';

const initialState = {
	feedMessages: [],
	groupMessages: [],
	chatGroups: {},
	searchString: "",
	searchResults: [],
	isGettingMessages: false,
	expandedMessage: {
		attrs: {
			_id: null,
			content: convertToRaw(EditorState.createEmpty().getCurrentContent())
		}
	}
}

const branchTable = {
	[actions.GETTING_MESSAGES]: (state, action) => {
		if (!state.chatGroups[action.chatGroupId]) {
			return {
				...state, 
				chatGroups: {
					...state.chatGroups,
					[action.chatGroupId]: []
				},
				isGettingMessages: true
			}
		} else {
			return {...state};
		}
	},
	[actions.RECEIVED_FEED_MESSAGES]: (state, action) => {
		let hasMore = true;
		if (action.payload.length === 0) {
			hasMore = false;
		}

		const hash = {};
		return {
			...state,
			isGettingMessages: false,
			feedMessages: [...state.feedMessages, ...action.payload].filter((v, i, s) => {
				if (!Object.keys(hash).includes(v._id)){
					hash[v._id] = 0;
					return true;
				} else {
					return false;
				}
			}),
			feedHasMore: hasMore
		}
	},
	[actions.SET_SEARCH_STRING]: (state, action) => {

		return {
			...state, 
			searchString: action.payload,
			searchResults: []
		}
	},
	[actions.RECEIVED_SEARCHED_MESSAGES]: (state, action) => {
		let hasMore = true;
		if (action.payload.length === 0) {
			hasMore = false;
		}

		const hash = {};

		return {
			...state, 
			searchResults: [...state.searchResults, ...action.payload].filter((v, i, s) => {
					if (!Object.keys(hash).includes(v._id)){
						hash[v._id] = 0;
						return true;
					} else {
						return false;
					}
				}),
			hasMore
		}
	},
	[actions.RECEIVED_MESSAGES]: (state, action) => {
		let hasMore = true;
		if (action.payload.length === 0) {
			hasMore = false;
		}

		const hash = {};
		return {
			...state,
			isGettingMessages: false,
			// groupMessages: [...state.groupMessages, ...action.payload].filter((v, i, s) => {
			// 	if (!Object.keys(hash).includes(v._id)){
			// 		hash[v._id] = 0;
			// 		return true;
			// 	} else {
			// 		return false;
			// 	}
			// }),
			chatGroups: {
				...state.chatGroups, 
				[action.chatGroupId]: [...action.payload, ...state.chatGroups[action.chatGroupId]]
			},
			hasMore
		}
	},
	[actions.RECEIVED_GROUP_MESSAGES]: (state, action) => {
		let hasMore = true;
		if (action.payload.length === 0) {
			hasMore = false;
		}

		const hash = {};
        let chatGroups = {...state.chatGroups};

		if (!state.chatGroups[action.chatGroupId]) {
			chatGroups[action.chatGroupId] = [];
		}
		return {
			...state,
			isGettingMessages: false,
			// chatGroups: {
			// 	...state.chatGroups, 
			// 	[action.chatGroupId]: [...state.chatGroups[action.chatGroupId], ...action.payload].filter((v, i, s) => {
			// 		if (!object.keys(hash).includes(v._id)){
			// 			hash[v._id] = 0;
			// 			return true;
			// 		} else {
			// 			return false;
			// 		}
			// 	})
			// },
			chatGroups: {
				...chatGroups, 
				[action.chatGroupId]: [...action.payload, ...chatGroups[action.chatGroupId]].filter((v, i, s) => {
					if (!Object.keys(hash).includes(v._id)){
						hash[v._id] = 0;
						return true;
					} else {
						return false;
					}
				})
			},
			groupHasMore: hasMore
		}
	},
	[actions.MESSAGE_CREATED]: (state, action) => {
        let chatGroups = {...state.chatGroups};
		if (!state.chatGroups[action.chatGroupId]) {
			chatGroups[action.chatGroupId] = [];
		}
		return {
			...state,
			chatGroups: {
				...chatGroups, 
				[action.chatGroupId]: [...chatGroups[action.chatGroupId], action.payload]
			}
		}
	},
	[actions.RECEIVED_MESSAGE]: (state, action) => {
        let chatGroups = {...state.chatGroups};
		if (!state.chatGroups[action.chatGroupId]) {
			chatGroups[action.chatGroupId] = [];
		}
		return {
			...state,
			chatGroups: {
				...chatGroups, 
				[action.chatGroupId]: [...chatGroups[action.chatGroupId], action.payload]
			}
		}
	},
	[actions.SET_EXPANDED_MESSAGE]: (state, action) => {
		return {
			...state,
			expandedMessage: action.payload
		}
	},
	[actions.MESSAGE_UPDATED]: (state, action) => {
		return {
			...state,
			expandedMessage: action.payload
		}
	},
	[actions.DELETED_MESSAGE]: (state, action) => {
		const deletedMessage = action.payload;
		const {chatGroupId}  = deletedMessage.attrs;
		return {
			...state,
			chatGroups: {
				...state.chatGroups, 
				[chatGroupId]: [...state.chatGroups[chatGroupId]].filter(post => post._id !== deletedMessage._id)
			}
		}
	}
}

export default (state = initialState, action) => {
    return action.type in branchTable ? branchTable[action.type](state, action) : state;
}
