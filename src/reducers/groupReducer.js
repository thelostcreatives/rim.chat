import { ChatGroup } from '../models';
import * as actions from '../actions';

const initialState = {
	activeGroup: {
		attrs: {
			signingKeyId: null
		}
	},
	isCreatingNewConvo: false,
	profileGroups: [],
	allGroups: {},
    chatGroups: [],
    selectedUsers: [],
    gettingInvitations: false,
    invitations: [],
    onlineUsers: {},
    gettingInvitationsBy: false,
    invitationsBy: [],
	uploadingBanner: false,
	deletingGroup: false
}

const branchTable = {
    [actions.GROUP_CREATED]: (state, action) => {
        return {
            ...state, 
			selectedUsers: []
        }
    },
	[actions.SET_ACTIVE_GROUP]: (state, action) => {
		if (action.payload) { 
            if (action.prependToChatGroups) {
                return {
                    ...state,
                    activeGroup: action.payload,
                    chatGroups: [action.payload, ...state.chatGroups]
                }

            }
            return {
                ...state,
                activeGroup: action.payload
            }
		}
        return { 
            ...state, activeGroup: initialState.activeGroup
        }
	},
	[actions.TOGGLE_CREATING_NEW_CONVO]: (state, action) => {
        if (state.isCreatingNewConvo) {
            return {
                ...state,
                selectedUsers: [],
                isCreatingNewConvo: !state.isCreatingNewConvo
            }

        }
		return {
            ...state,
            activeGroup: initialState.activeGroup,
			isCreatingNewConvo: !state.isCreatingNewConvo
		}
	},
    [actions.UPDATE_ONLINE_USERS]: (state, action) => {
		return {
			...state,
            onlineUsers: {
                ...state.onlineUsers,
                [action.groupId]: action.payload
            }
		}
    },
    [actions.SELECT_USER]: (state, action) => {
		return {
			...state,
			selectedUsers: [...state.selectedUsers, action.payload]
		}
    },
    [actions.DESELECT_USER]: (state, action) => {

        const newList = state.selectedUsers.filter((user, idx) => idx !== action.payload);

		return {
			...state,
			selectedUsers: newList
		}
	},
    [actions.GETTING_GROUP_INVITATIONS]: (state, action) => {
        return {
            ...state,
            gettingInvitations: true
        }
	},
    [actions.GROUP_INVITATIONS_RECEIVED]: (state, action) => {
        return {
            ...state,
            invitations: [...action.payload],
            gettingInvitations: false
        }
	},
    [actions.GETTING_GROUP_INVITATIONS_BY]: (state, action) => {
        return {
            ...state,
            gettingInvitationsBy: true
        }
	},
    [actions.GROUP_INVITATIONS_BY_RECEIVED]: (state, action) => {
        return {
            ...state,
            invitationsBy: [...action.payload],
            gettingInvitationsBy: false
        }
	},
    [actions.CLEAR_INVITATIONS_BY]: (state, action) => {
        return {
            ...state,
            invitationsBy: [],
        }
	},
	[actions.GROUPS_RECEIVED]: (state, action) => {
		if (action.payload) { 
			return {
				...state, 
				chatGroups: [...action.payload]
			}
		} else { 
			return { 
				...state
			}
		}
		
	},
    [actions.GROUP_RECEIVED]: (state, action) => {
        //if (action.payload && state.isCreatingNewConvo) { 
		//	return {
		//		...state, 
		//		chatGroups: [action.payload, ...state.chatGroups]
		//	}
		//} else { 
		//	return { 
		//		...state
		//	}
		//}
        return { 
            ...state
        }
		
	},
    [actions.INVITATION_ACTIVATED]: (state, action) => {
		if (action.payload) { 
			return {
				...state, 
                invitations: state.invitations.filter((invitation) => invitation._id !== action.payload._id)
			}
		} else { 
			return { 
				...state
			}
		}
		
	},
	[actions.GROUP_DATA_RECEIVED]: (state, action) => {
		if (action.payload) { 
			return {
				...state, 
				allGroups: {
					...state.allGroups,
					[action.payload._id]: action.payload
				}
			}
		} else { 
			return { 
				...state
			}
		}
		
	},
	[actions.RECEIVED_PROFILE_GROUPS]: (state, action) => {
		return {
			...state,
			profileGroups: [...action.payload]
		}
	},
	[actions.UPLOADING_GROUP_BANNER]: (state, action) => {
		return {
			...state, uploadingBanner: true
		}
	},
	[actions.GROUP_UPDATED]: (state, action) => {
		return {
			...state,
			uploadingBanner: false,
			activeGroup: action.payload
		}
	},
	[actions.DELETING_GROUP]: (state, action) => {
		return {
			...state,
			deletingGroup: true
		}
	},
	[actions.GROUP_DELETED]: (state, action) => {
		return {
			...state,
			profileGroups: [...state.profileGroups].filter((list) => list._id !== action.payload._id),
			deletingGroup: false
		}
	}
}

export default (state = initialState, action) => {
    return action.type in branchTable ? branchTable[action.type](state, action) : state;
}
