import { combineReducers } from 'redux';

import auth from './authReducer';
import tracks from './tracksReducer';
import sessions from './sessionsReducer';
import notifs from './notifsReducer';
import message from './messageReducer';
import group from './groupReducer';

export default combineReducers({
    auth,
    group,
    message,
    tracks,
    sessions,
    notifs
});
