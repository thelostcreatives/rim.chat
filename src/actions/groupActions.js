import { GroupInvitation } from 'radiks-x';
import { ChatGroup, Post, Invitation } from '../models';
import { USER_UPDATED } from './index';
import {
    uploadFile, isImageFileSizeAcceptable, compressImage,
    descendSortModelsByUpdatedAt
} from '../utils/helpers';

export const CREATING_GROUP = "CREATING_GROUP";
export const GROUP_CREATED = "GROUP_CREATED";

export const INVITING_MEMBERS = "INVITING_MEMBERS";
export const MEMBERS_INVITED = "MEMBERS_INVITED";

export const GETTING_GROUPS = "GETTING_GROUPS";
export const GROUPS_RECEIVED = "GROUPS_RECEIVED";

export const GETTING_GROUP = "GETTING_GROUP";
export const GROUP_RECEIVED = "GROUP_RECEIVED";

export const GETTING_GROUP_INVITATIONS = "GETTING_GROUP_INVITATION";
export const GROUP_INVITATIONS_RECEIVED = "GROUP_INVITATION_RECEIVED";

export const GETTING_GROUP_INVITATIONS_BY = "GETTING_GROUP_INVITATIONS_BY";
export const GROUP_INVITATIONS_BY_RECEIVED = "GROUP_INVITATIONS_BY_RECEIVED";

export const CLEAR_INVITATIONS_BY = "CLEAR_INVITATIONS_BY";

export const ACTIVATING_INVITATION = "ACTIVATING_INVITATION";
export const INVITATION_ACTIVATED = "INVITATION_ACTIVATED";

export const UPDATING_INVITATION = "UPDATING_INVITATION";
export const INVITATION_UPDATED = "INVITATION_UPDATED";

export const GETTING_GROUP_DATA = "GETTING_GROUP_DATA";
export const GROUP_DATA_RECEIVED = "GROUP_DATA_RECEIVED";

export const GETTING_PROFILE_GROUPS = "GETTING_PROFILE_GROUPS";
export const RECEIVED_PROFILE_GROUPS = "RECEIVED_PROFILE_GROUPS";

export const UPDATE_ONLINE_USERS = "UPDATE_ONLINE_USERS";

export const SET_ACTIVE_GROUP = "SET_ACTIVE_GROUP";

export const TOGGLE_CREATING_NEW_CONVO = "TOGGLE_CREATING_NEW_CONVO";

export const SELECT_USER = "SELECT_USER";
export const DESELECT_USER = "DESELECT_USER";

export const ADDING_GROUP_TO_FOLLOWS = "ADDING_GROUP_TO_FOLLOWS";
export const GROUP_ADDED_TO_FOLLOWS = "GROUP_ADDED_TO_FOLLOWS";
export const REMOVING_GROUP_FROM_FOLLOWS = "REMOVING_GROUP_FROM_FOLLOWS";

export const UPDATING_GROUP = "UPDATING_GROUP";
export const GROUP_UPDATED = "GROUP_UPDATED";

export const DELETING_GROUP = "DELETING_GROUP";
export const GROUP_DELETED = "GROUP_DELETED";

export const UPLOADING_GROUP_BANNER = "UPLOADING_GROUP_BANNER";
export const GROUP_BANNER_UPLOADED = "GROUP_BANNER_UPLOADED";

export const createGroup = (name, description, members) => async (dispatch) => {
    dispatch({
        type: CREATING_GROUP
    });

	const newGroup = new ChatGroup({
		name,
        description,
	});

    const group = await newGroup.create();

    dispatch({
        type: GROUP_CREATED,
        payload: group
    });

    return group;
}

export const getGroups = () => async (dispatch) => {
    dispatch({
        type: GETTING_GROUPS
    });

    const groups = await ChatGroup.myGroups();

    dispatch({
        type: GROUPS_RECEIVED,
        payload: groups.sort(descendSortModelsByUpdatedAt)
    });

    return groups;
}

export const getGroup = (groupId) => async (dispatch) => {
    dispatch({
        type: GETTING_GROUP
    });

    const group = await ChatGroup.find(groupId);

    dispatch({
        type: GROUP_RECEIVED,
        payload: group
    });

    return group;
}

export const inviteMembers = ( author, group, members) => async (dispatch) => {
    dispatch({
        type: INVITING_MEMBERS
    });

    // async reduce from: https://stackoverflow.com/questions/55225272/map-function-with-async-await/55225745#55225745
    const invitations = await members.reduce(async (previous, member) => {
        const result = await previous;

        const invitation = await group.makeGroupMembership(member);

        const customInvitation = await new Invitation({
            userGroupId: group._id,
            by: author,
            for: member,
            invitationId: invitation._id
        });

        await customInvitation.save();
        return [...result, invitation];
    }, Promise.resolve([]));


    dispatch({
        type: MEMBERS_INVITED   
    });

    return group;
}

export const getGroupInvitations = (username) => async (dispatch) => {
    dispatch({
        type: GETTING_GROUP_INVITATIONS
    });

    const invitations = await Invitation.fetchList({
        for: username,
        activated: false
    });

    dispatch({
        type: GROUP_INVITATIONS_RECEIVED,
        payload: invitations
    });
}

export const getGroupInvitationsBy = (by, username) => async (dispatch) => {
    dispatch({
        type: GETTING_GROUP_INVITATIONS_BY
    });

    const invitations = await Invitation.fetchList({
        by,
        for: username,
        activated: false
    });

    dispatch({
        type: GROUP_INVITATIONS_BY_RECEIVED,
        payload: invitations
    });
}

export const clearInvitationsBy = () => (dispatch) => {
    dispatch({
        type: CLEAR_INVITATIONS_BY
    });
}

export const activateInvitation = (invitation) => async (dispatch) =>  {
    dispatch({
        type: ACTIVATING_INVITATION
    });

    const { invitationId } = invitation.attrs;

    const groupInvitation = await GroupInvitation.findById(invitationId);

    await groupInvitation.activate();

    //invitation.update({
    //    activated: true
    //});

    //await invitation.save();

    dispatch({
        type: INVITATION_ACTIVATED,
        payload: invitation
    });
}

export const updateInvitation = (invitation, updates) => async (dispatch) => {
    dispatch({
        type: UPDATING_INVITATION
    });

    invitation.update(updates);

    await invitation.save();

    dispatch({
        type: INVITATION_UPDATED
    });
}

export const getGroupData = (listId) => async (dispatch) => {
    dispatch({
        type: GETTING_GROUP_DATA
    });

    const list = await ChatGroup.fetchGroup({
        _id: listId
    });

    dispatch({
        type: GROUP_DATA_RECEIVED,
        payload: list[0]
    });
}

export const getProfileGroups = (username) => async (dispatch) => {

    dispatch({
        type: GETTING_PROFILE_GROUPS,
    });

    if (username) {
        const profileGroups = await ChatGroup.fetchGroup({
            author: username
        });

        dispatch({
            type: RECEIVED_PROFILE_GROUPS,
            payload: profileGroups,
        });
    } else {
        dispatch({
            type: RECEIVED_PROFILE_GROUPS,
            payload: [],
        });
    }
    
}

export const updateOnlineUsers = (groupId, users) => (dispatch) => {
    dispatch({
        type: UPDATE_ONLINE_USERS,
        payload: users,
        groupId
    });
}

export const setActiveGroup = (group, prependToChatGroups = false) => {
	return {
		type: SET_ACTIVE_GROUP,
        payload: group,
        prependToChatGroups
	}
}

export const toggleCreatingNewConvo = () => {
    return {
        type: TOGGLE_CREATING_NEW_CONVO
    }
}

export const selectUser = (username) => {
    return {
        type: SELECT_USER,
        payload: username
    }
}

export const deselectUser = (idx) => {
    return {
        type: DESELECT_USER,
        payload: idx
    }
}

export const followGroup = (anylistUser, listId) => async (dispatch) => {
    dispatch({
        type: ADDING_GROUP_TO_FOLLOWS
    });

    const follows = [...anylistUser.attrs.followedGroups, listId];

    anylistUser.update({
        followedGroups: follows.filter((v, i, s) => s.indexOf(v) === i)
    });

    const updatedUser = await anylistUser.save();

    dispatch({
        type:   USER_UPDATED,
        payload: updatedUser
    });
}

export const unfollowGroup = (anylistUser, listId) => async (dispatch) => {
    dispatch({
        type: REMOVING_GROUP_FROM_FOLLOWS
    });

    const { followedGroups } = anylistUser.attrs;
    followedGroups.splice(followedGroups.indexOf(listId), 1);

    anylistUser.update({
        followedGroups: [...followedGroups]
    });

    const updatedUser = await anylistUser.save();

    dispatch({
        type: USER_UPDATED,
        payload: updatedUser 
    })
}

export const updateGroup = (list, updates) => async (dispatch) => {
    dispatch({
        type: UPDATING_GROUP
    });

    list.update(updates);

    const updatedGroup = await list.save();

    dispatch({
        type: GROUP_UPDATED,
        payload: updatedGroup
    });
}

export const deleteGroup = (list, redirect) => async (dispatch) => {
    dispatch({
        type: DELETING_GROUP
    });

    const posts = await Post.fetchGroup({
        listId: list._id
    });

    await Promise.all(posts.map(post => post.destroy()));

    await list.destroy();

    dispatch({
        type: GROUP_DELETED,
        payload: list
    });

    redirect();
}

export const uploadBanner = (userSession, list, file) => async (dispatch) => {
    dispatch({
        type: UPLOADING_GROUP_BANNER
    });

    let link;

    const process = async () => {
        list.update({
            other: {
                bannerLink: link
            }
        });

        const updatedGroup = await list.save();

        dispatch({
            type: GROUP_UPDATED,
            payload: updatedGroup
        });
    }

    if (isImageFileSizeAcceptable(file.size)) {
        link = await uploadFile(userSession, "listBanners", file, {encrypt:false});
        process();
    } else {
        compressImage(file, async (compressed) => { 
            file = compressed;
            link = await uploadFile(userSession, "listBanners", file, {encrypt:false});
            process();
        });
    }
}
