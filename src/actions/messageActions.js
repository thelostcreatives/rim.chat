import { Message } from '../models';
import { USER_UPDATED } from './index';
import { uploadFile, HANDLE_REGEX, HASHTAG_REGEX, getMatchesFromString, descendSortModelsByCreatedAt } from '../utils/helpers';

export const CREATING_MESSAGE = "CREATING_MESSAGE";
export const MESSAGE_CREATED = "MESSAGE_CREATED";
export const RECEIVED_MESSAGE = "RECEIVED_MESSAGE";

export const GETTING_MESSAGES = "GETTING_MESSAGES";
export const RECEIVED_MESSAGES = "RECEIVED_MESSAGES";
export const RECEIVED_GROUP_MESSAGES = "RECEIVED_GROUP_MESSAGES";

export const UPDATING_MESSAGE = "UPDATING_MESSAGE";
export const MESSAGE_UPDATED = "MESSAGE_UPDATED";

export const DELETING_MESSAGE = "DELETING_MESSAGE";
export const DELETED_MESSAGE = "DELETED_MESSAGE";

export const GETTING_FEED_MESSAGES = "GETTING_FEED_MESSAGES";
export const RECEIVED_FEED_MESSAGES = "RECEIVED_FEED_MESSAGES";

export const SET_EXPANDED_MESSAGE = "SET_EXPANDED_MESSAGE";

export const ADDING_MESSAGE_TO_FOllOWS = "ADDING_MESSAGE_TO_FOllOWS";
export const REMOVING_MESSAGE_FROM_FOLLOWS = "REMOVING_MESSAGE_FROM_FOLLOWS";

export const UPLOADING_IMAGES = "UPLOADING_IMAGES";
export const IMAGES_UPLOADED = "IMAGES_UPLOADED";

export const UPLOADING_VIDEO = "UPLOADING_VIDEO";
export const VIDEO_UPLOADED = "VIDEO_UPLOADED";

export const UPLOADING_AUDIO = "UPLOADING_AUDIO";
export const AUDIO_UPLOADED = "AUDIO_UPLOADED";

export const SET_SEARCH_STRING = "SET_SEARCH_STRING";

export const SEARCHING_MESSAGES = "SEARCHING_MESSAGES";
export const RECEIVED_SEARCHED_MESSAGES = "RECEIVED_SEARCHED_MESSAGES";

export const createMessage = (chatGroupId, author, content, mentions, hashtags, imgs, video, audio) => async (dispatch) => {
    dispatch({
        type: CREATING_MESSAGE
	});

	const newMessage = new Message({
		userGroupId: chatGroupId,
		author,
		content,
		mentions,
		hashtags,
		other: {
			images: imgs,
			video,
			audio
		}
	});

	newMessage.save();

    dispatch({
		type: MESSAGE_CREATED,
		payload: newMessage,
		chatGroupId
    });

	return newMessage;
}

export const receiveMessage = (message) => async (dispatch) => {
	dispatch({
		type: RECEIVED_MESSAGE,
		payload: message,
		chatGroupId: message.attrs.userGroupId
	});
}

export const getMessages = (offset, limit = 5, chatGroupId) => async (dispatch) => {
	dispatch({
		type: GETTING_MESSAGES,
		chatGroupId
	});

	let newMessages;

	if (chatGroupId) {
		newMessages = await Message.fetchList({
			userGroupId: chatGroupId,
			offset,
			limit,
			sort: '-createdAt'
		});

		dispatch({
			type: RECEIVED_GROUP_MESSAGES,
			payload: newMessages.reverse(),
			chatGroupId
		});
	} else {
		newMessages = await Message.fetchList({
			offset,
			limit,
			sort: '-createdAt'
		});

		dispatch({
			type: RECEIVED_MESSAGES,
			payload: newMessages.reverse()
		});
	}
}

export const getFeedMessages = (followedLists, offset, limit = 5) => async (dispatch) => {
	dispatch({
		type: GETTING_FEED_MESSAGES 
	}); 

	if (followedLists.length > 0) {
		const newMessages = await Message.fetchList({
			offset,
			limit,
			userGroupId: followedLists,
			sort: '-createdAt'
		});

		dispatch({
			type: RECEIVED_FEED_MESSAGES,
			payload: newMessages
		});
	} else {
		dispatch({
			type: RECEIVED_FEED_MESSAGES,
			payload: []
		});
	}
}

export const setSearchString = (searchString) => { 
	return {
		type: SET_SEARCH_STRING,
		payload: searchString
	}
}

export const searchMessages = (searchString, offset, limit = 5) => async (dispatch) => { 
	dispatch({ 
		type: SEARCHING_MESSAGES
	});

	const mentions = getMatchesFromString(HANDLE_REGEX, searchString).map(mention => mention.substr(1));
	const hashtags = getMatchesFromString(HASHTAG_REGEX, searchString).map(hashtag => hashtag.substr(1));

	let postsWithMentions = [];
	let postsWithHashtags = [];

	if (mentions.length > 0) {
		postsWithMentions = await Message.fetchList({ 
			offset,
			limit,
			mentions: mentions,
			sort: '-createdAt'
		});
	}

	if (hashtags.length > 0) { 
		postsWithHashtags = await Message.fetchList({ 
			offset,
			limit,
			hashtags: hashtags,
			sort: '-createdAt'
		});
	}

	const searchResults = [...postsWithMentions, ...postsWithHashtags].sort(descendSortModelsByCreatedAt);

	dispatch({ 
		type: RECEIVED_SEARCHED_MESSAGES,
		payload: searchResults
	});
}


export const setExpandedMessage = (post) => {
	return {
		type: SET_EXPANDED_MESSAGE,
		payload: post
	}
}

export const updateMessage = (post, content, mentions, hashtags) => async (dispatch) => {
	dispatch({
		type: UPDATING_MESSAGE
	});

	post.update({
		content,
		mentions,
		hashtags
	});

	const updatedMessage = await post.save();

	dispatch({
		type: MESSAGE_UPDATED,
		payload: updatedMessage
	});
}

export const deleteMessage = (post) => async (dispatch) => {
	dispatch({
		type: DELETING_MESSAGE
	});

	await post.destroy();

	dispatch({
		type: DELETED_MESSAGE,
		payload: post
	});

}

export const followMessage = (anylistUser, postId) => async (dispatch) => {
	dispatch({
		type: ADDING_MESSAGE_TO_FOllOWS
	});

	const posts = [...anylistUser.attrs.followedMessages, postId];

    anylistUser.update({
        followedMessages: posts.filter((v, i, s) => s.indexOf(v) === i)
    });

    const updatedUser = await anylistUser.save();

    dispatch({
        type:   USER_UPDATED,
        payload: updatedUser
    });
}

export const unfollowMessage = (anylistUser, postId) => async (dispatch) => {
    dispatch({
        type: REMOVING_MESSAGE_FROM_FOLLOWS
    });

    const { followedMessages } = anylistUser.attrs;
    followedMessages.splice(followedMessages.indexOf(postId), 1);

    anylistUser.update({
        followedMessages: [...followedMessages]
    });

    const updatedUser = await anylistUser.save();

    dispatch({
        type: USER_UPDATED,
        payload: updatedUser 
    });
}

export const uploadImages = (userSession, user, images) => async (dispatch) => {
	dispatch({
		type: UPLOADING_IMAGES
	});

	const links = await Promise.all(images.map(image => uploadFile(userSession, "img_posts", image, {encrypt:false})));

    dispatch({
        type: IMAGES_UPLOADED,
	});
	
	return links;
}

export const uploadVideo = (userSession, user, video) => async (dispatch) => {
	dispatch({
		type: UPLOADING_VIDEO
	});

	const link = await uploadFile(userSession, "video_posts", video, {encrypt:false});

    dispatch({
        type: VIDEO_UPLOADED,
	});
	
	return link;
}

export const uploadAudio = (userSession, user, audio) => async (dispatch) => {
	dispatch({
		type: UPLOADING_AUDIO
	});

	const link = await uploadFile(userSession, "audio_posts", audio, {encrypt:false});

    dispatch({
        type: AUDIO_UPLOADED,
	});
	
	return link;
}
