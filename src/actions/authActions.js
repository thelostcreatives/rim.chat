import  { RimUser } from '../models';
import { stringifyMembers } from '../utils/helpers';

export const SIGNIN= "SIGNIN";
export const SIGNOUT = "SIGNOUT";
export const STOREUSERSESSION = "STOREUSERSESSION";

export const SETTING_SOCKET = "SETTING_SOCKET";

export const UPDATING_CHATGROUPREF = "UPDATING_CHATGROUPREF";

export const GETTING_CUSTOM_USER = "GETTING_CUSTOM_USER";
export const CUSTOM_USER_FOUND = "CUSTOM_USER_FOUND";

export const SEARCHING_USER = "SEARCHING_USER";
export const USER_FOUND = "USER_FOUND";

export const GETTING_USER_DATA = "GETTING_USER_DATA";
export const USER_DATA_RECEIVED = "USER_DATA_RECEIVED";

export const UPDATING_USER = "UPDATING_USER";
export const USER_UPDATED = "USER_UPDATED";

export const SET_ACTIVE_PROFILE = "SET_ACTIVE_PROFILE";

export const UPLOADING_AVATAR = "UPLOADING_AVATAR";

//export auth methods below
export function handleSignIn(e, userSession) {
    e.preventDefault();
    userSession.redirectToSignIn();
    return {
        type: SIGNIN
    }
}

export function handleSignOut(e, userSession) {
    if (e.preventDefault) {
        e.preventDefault();
    }

    userSession.signUserOut(window.location.origin);
    return {
        type: SIGNOUT
    }
}

export function storeUserSession(userSession) {
    return {
        type: STOREUSERSESSION,
        payload: userSession
    }
}

export const setSocket = (socket) => {
    return {
        type: SETTING_SOCKET,
        payload: socket
    }

}

export const getCustomUser = ({profile, username}) => async (dispatch) => {
    dispatch({
        type: GETTING_CUSTOM_USER
    });

    const exists = await RimUser.fetchList({
        username,
        sort: 'createdAt'
    });

    let user;

    const defaultGroupKeys = JSON.parse(localStorage.getItem('GROUP_MEMBERSHIPS_STORAGE_KEY'));

    if (exists.length > 0) {
        user = exists[0];
    } else {
        const newuser = new RimUser({
            name: profile.name,
            username,
            description: profile.description,
            // GROUP_MEMBERSHIPS_STORAGE_KEY: defaultGroupKeys,
            other : {
                avatarUrl: profile.image ? profile.image[0].contentUrl : null,
                lastSeenNotif: Date.now()
            }
        });

        const res = await newuser.save();
        user = res;
    }

    if (user.attrs.GROUP_MEMBERSHIPS_STORAGE_KEY) {
        localStorage.setItem('GROUP_MEMBERSHIPS_STORAGE_KEY', JSON.stringify(user.attrs.GROUP_MEMBERSHIPS_STORAGE_KEY));
    } else {
        user.update({
            GROUP_MEMBERSHIPS_STORAGE_KEY: defaultGroupKeys
        });
        user.save();
    }

    dispatch({
        type: CUSTOM_USER_FOUND,
        payload: user
    });
}

export const searchUser = (username) => async (dispatch) => {
    dispatch({
        type: SEARCHING_USER
    });

    const searchResults = await RimUser.fetchList({
        username: `/${username}/`,
        limit: 5
    });

    if (username.length > 0) {
        dispatch({
            type: USER_FOUND,
            payload: searchResults
        });
    } else {
        dispatch({
            type: USER_FOUND,
            payload: []
        });
    }
}

export const getUserData = (username) => async (dispatch) => {
    dispatch({
        type: GETTING_USER_DATA
    });

    const user = await RimUser.fetchList({
        username,
        sort: 'createdAt'
    });

    dispatch({
        type: USER_DATA_RECEIVED,
        payload: user[0]
    });
}


export const updateUser = (user, updates) => async (dispatch) => {
    dispatch({
        type: UPDATING_USER
    });

    user.update(updates);

    await user.save();

    const updatedUser = new RimUser(user.attrs);

    dispatch({
        type: USER_UPDATED,
        payload: updatedUser
    });
}

export const updateChatGroupRef = (user, chatGroup) => async (dispatch) => {
    dispatch({
        type: UPDATING_CHATGROUPREF
    });

    const currentRef = user.attrs.chatGroupRef;
    const memberString = stringifyMembers(chatGroup.attrs.members.map(member => member.username));

    user.update({
        chatGroupRef: {
            ...currentRef,
            [memberString]: chatGroup._id
        }
    });

    const updatedUser = await user.save();

    dispatch({
        type: USER_UPDATED,
        payload: updatedUser
    });
}
