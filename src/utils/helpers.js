import { CompositeDecorator } from 'draft-js';
import { without } from 'underscore';

import { IMAGE_FILE_SIZE_LIMIT } from './constants';
import { Highlighter, InternalLink, ExternalLink } from '../components';

export const isLicenseValid = async (license) => { 
	let url = 'https://gumroad-validation.herokuapp.com/validate';

	if (!license) { 
		return false;
	}
	
	const licenseResponse = await fetch(url, {
		method: 'POST',
		mode: 'cors',
		body: JSON.stringify({
			product_permalink: 'ToxHN', //change this later
			license_key: license
		}),
		headers:{
			'Content-Type': 'application/json'
		}
	});

	if (licenseResponse.status === 404) {
		return false
	}
	
	const data = await licenseResponse.json();
	const purchaseData = data.purchase;

	if (!purchaseData.subscription_cancelled_at && !purchaseData.subscription_failed_at) { 
		return true;
	} else { 
		return false;
	}
}

export const uploadFile = async (userSession, dir, file, options) => {
	const gaialink = await userSession.putFile(`${dir}/${file.name}`, file, options);
	return gaialink;
}

/**
 * Check if image file size is <= 50 kb
 */
export const isImageFileSizeAcceptable = (fileSize) => {
	if ( fileSize  <= IMAGE_FILE_SIZE_LIMIT ) {
		return true;
	} else {
		return false;
	}
}

export const areAllImageFileSizesAcceptable = (images) => {
	const imageEvals = images.map(image => isImageFileSizeAcceptable(image.size));
	const allAcceptable = imageEvals.every((val) => val === true);
	return allAcceptable;
}

// referenced: https://medium.com/@chaman.k/compress-resize-and-manage-images-using-javascript-directly-from-the-browser-2a2bc08b6c5d
export const compressImage = (imgFile, callback) => {
	const quality = IMAGE_FILE_SIZE_LIMIT / imgFile.size;
	
    const fileName = imgFile.name;
    const reader = new FileReader();
    reader.readAsDataURL(imgFile);
    reader.onload = event => {
        const img = new Image();
		img.src = event.target.result;
		
        img.onload = () => {
			const width = img.width;
			const height = img.height ;

			const elem = document.createElement('canvas');
			elem.width = width;
			elem.height = height;
			const ctx = elem.getContext('2d');

			ctx.drawImage(img, 0, 0, width, height);
			ctx.canvas.toBlob((blob) => {
				const file = new File([blob], fileName, {
					type: 'image/jpeg',
					lastModified: Date.now()
				});

				img.src = URL.createObjectURL(file);

				img.onload = function() {
					// no longer need to read the blob so it's revoked
					URL.revokeObjectURL(img.src);
				};

				//compress again in cases where it still larger than the limit
				if (file.size > IMAGE_FILE_SIZE_LIMIT) {
					compressImage(file, callback);
				} else {
					callback(file);
				}
			}, 'image/jpeg', quality);
		};
		reader.onerror = error => console.log(error);
	};
}

export const removeExtraNewLines = (plainText) => {
	const cleanText = plainText.split("\n").filter((text, idx, arr) => {
		let returnVal;
		if (idx > 0 && idx < arr.length - 1) {
			if (text.length > 0 || arr[idx + 1].length > 0) {
				returnVal = true;
			}
		} else if (idx === arr.length - 1 || idx === 0) {
			returnVal = true;
		} else {
			returnVal = false;
		}
		return returnVal;
	}).join('\n');

	return cleanText;
}

// from https://erictarn.com/post/1060722347/the-best-twitter-hashtag-regular-expression
export const HANDLE_REGEX = /\B@\w*[a-zA-Z\.]+\w*/g;
export const HASHTAG_REGEX = /\B#\w*[a-zA-Z]+\w*/g;
// modified from https://www.regextester.com/96504
export const LINK_REGEX = /(?:(?:https?):\/\/|\b(?:[a-z\d]+\.\w*[^\.\.\s"'\[\]]))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?/g;

export const findWithRegex = (regex, contentBlock, callback) => {
	const text = contentBlock.getText();
	let matchArr, start;
	while ((matchArr = regex.exec(text)) !== null) {
		start = matchArr.index;
		callback(start, start + matchArr[0].length);
	}
}

export const getMatchesFromString = (regex, string) => {
	const matches = [];
	let matchArr;

	while ((matchArr = regex.exec(string)) !== null) {
		matches.push(matchArr[0]);
	}
	return matches;
}

export const handleStrategy = (contentBlock, callback, contentState) => {
	findWithRegex(HANDLE_REGEX, contentBlock, callback);
}
export const hashtagStrategy = (contentBlock, callback, contentState) => {
	findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}
export const linkStrategy = (contentBlock, callback, contentState) => {
	findWithRegex(LINK_REGEX, contentBlock, callback);
}

/**
 * Current types:
 * 'editor': highlights hashtags, mentions and links
 * 'post': renders hashtags, mentions and links text with achor tags
 */
export const getCompositeDecorator = (type) => {
	const decorators = {
		editor: () => new CompositeDecorator([ 
			{ 
				strategy: handleStrategy,
				component: Highlighter
			},
			{ 
				strategy: hashtagStrategy,
				component: Highlighter
			},
			{ 
				strategy: linkStrategy,
				component: Highlighter
			}
		]),
		post: () => new CompositeDecorator([
			{
				strategy: handleStrategy,
				component: InternalLink,
			},
			{
				strategy: hashtagStrategy,
				component: InternalLink,
			},
			{
				strategy: linkStrategy,
				component: ExternalLink,
			}
		])
	};

	return decorators[type]();
}

// from https://stackoverflow.com/questions/11300906/check-if-a-string-starts-with-http-using-javascript
export const getValidUrl = (url = "") => {
    let newUrl = window.decodeURIComponent(url);
    newUrl = newUrl.trim().replace(/\s/g, "");

    if(/^(:\/\/)/.test(newUrl)){
        return `http${newUrl}`;
    }
    if(!/^(f|ht)tps?:\/\//i.test(newUrl)){
        return `http://${newUrl}`;
    }

    return newUrl;
};

// array.sort() callback to sort radiks models in descending order
export const descendSortModelsByCreatedAt = (a, b) => {
	if (a.attrs.createdAt > b.attrs.createdAt) {
		return -1;
	}

	if (a.attrs.createdAt < b.attrs.createdAt) {
		return 1;
	}

	return 0;
}

export const descendSortModelsByUpdatedAt = (a, b) => {
	if (a.attrs.updatedAt > b.attrs.updatedAt) {
		return -1;
	}

	if (a.attrs.updatedAt < b.attrs.updatedAt) {
		return 1;
	}

	return 0;
}

// chunk 1d array. https://medium.com/@Dragonza/four-ways-to-chunk-an-array-e19c889eac4
export const chunk = (array, size) => {
	if (!array) return [];

	const firstChunk = array.slice(0, size);
	if (!firstChunk.length) {
		return array;
	}
	return [firstChunk].concat(chunk(array.slice(size, array.length), size)); 
}

// code from: https://gomakethings.com/check-if-two-arrays-or-objects-are-equal-with-javascript/
export const isEqual = function (value, other) {

	// Get the value type
	var type = Object.prototype.toString.call(value);

	// If the two objects are not the same type, return false
	if (type !== Object.prototype.toString.call(other)) return false;

	// If items are not an object or array, return false
	if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

	// Compare the length of the length of the two items
	var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
	var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
	if (valueLen !== otherLen) return false;

	// Compare two items
	var compare = function (item1, item2) {

		// Get the object type
		var itemType = Object.prototype.toString.call(item1);

		// If an object or array, compare recursively
		if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
			if (!isEqual(item1, item2)) return false;
		}

		// Otherwise, do a simple comparison
		else {

			// If the two items are not the same type, return false
			if (itemType !== Object.prototype.toString.call(item2)) return false;

			// Else if it's a function, convert to a string and compare
			// Otherwise, just compare
			if (itemType === '[object Function]') {
				if (item1.toString() !== item2.toString()) return false;
			} else {
				if (item1 !== item2) return false;
			}

		}
	};

	// Compare properties
	if (type === '[object Array]') {
		for (var i = 0; i < valueLen; i++) {
			if (compare(value[i], other[i]) === false) return false;
		}
	} else {
		for (var key in value) {
			if (value.hasOwnProperty(key)) {
				if (compare(value[key], other[key]) === false) return false;
			}
		}
	}

	// If nothing failed, return true
	return true;

};

export const getAltGroupName = (members = [], username) => {
    
	const memberUsernames = members.map(member => {
		return member.username;
	});

    return without(memberUsernames, username).join(', ');
}

export const stringifyMembers = (members) => {

    return members.sort().join('-');

}

export const updateChatGroupRef = (user, members, groupId) => {
    const currentRef = user.attrs.chatGroupRef;
    const memberString = stringifyMembers(members);

    const updatedChatGroupRef = {
        ...currentRef,
        [memberString]: groupId
    }

    return updatedChatGroupRef;
}

// from https://medium.com/javascript-in-plain-english/how-to-deep-copy-objects-and-arrays-in-javascript-7c911359b089
export const deepCopy = obj => {
  let clone, value, key;

  if(typeof obj !== "object" || obj === null) {
    return obj; // Return the value if obj is not an object
  }

  // Create an array or object to hold the values
  clone = Array.isArray(obj) ? [] : {};

  for (key in obj) {
    value = obj[key];

    // Recursively (deep) copy for nested objects, including arrays
    clone[key] = (typeof value === "object" && value !== null) ? deepCopy(value) : value;
  }
  
  return clone;
}
